package com.dudak.todo.Todo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Document(collection = "todos")
public class Todo {

    /*
      TODO:
     * add headline
     * add body
     * add notification date & time
     * add priority
     */

    @Id
    public String id;
    @Size(max = 255)
    private String headline;
    @NotNull
    @Size(min = 1, max = 20000)
    private String body;
    private Date created_datetime;
    private Date notification_datetime;

    public Todo(@Size(max = 255) String headline,
                @Size(min = 1, max = 20000) String body,
                @NotNull Date created_datetime,
                Date notification_datetime) {
        this.headline = headline;
        this.body = body;
        this.created_datetime = created_datetime;
        this.notification_datetime = notification_datetime;
    }
}
