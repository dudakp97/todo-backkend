package com.dudak.todo.Todo.controller;

import com.dudak.todo.Exceptions.ResourceNotFoundException;
import com.dudak.todo.Todo.model.Todo;
import com.dudak.todo.Todo.service.TodoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/todo")
public class TodoController {

    private final TodoServiceImpl todoService;

    @Autowired
    public TodoController(TodoServiceImpl todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<Todo> getAllTodo() {
        return todoService.getAllTodo();
    }

    @PostMapping
    public void createTodo(String headline, String body, String dateTimeNotification) throws ParseException {
        todoService.createTodo(headline, body, dateTimeNotification);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteTodo(@PathVariable String id) throws ResourceNotFoundException {
        return new ResponseEntity(todoService.deleteTodoById(id) ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PutMapping
    public ResponseEntity updateTodo(String id, String headline, String body, String dateTimeNotification) throws ParseException {
        if(todoService.updateTodo(id, headline, body, dateTimeNotification))
            return new ResponseEntity(HttpStatus.OK);
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
