package com.dudak.todo.Todo.service;

import com.dudak.todo.Author.repository.AuthorRepository;
import com.dudak.todo.Exceptions.ResourceNotFoundException;
import com.dudak.todo.Todo.model.Todo;
import com.dudak.todo.Todo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class TodoServiceImpl implements TodoService {

    private final TodoRepository todoRepository;
    private final AuthorRepository authorRepository;

    @Autowired
    public TodoServiceImpl(TodoRepository todoRepository, AuthorRepository authorRepository) {
        this.todoRepository = todoRepository;
        this.authorRepository = authorRepository;
    }


    @Override
    public List<Todo> getAllTodo() {
        return todoRepository.findAll();
    }

    @Override
    public void createTodo(String headline, String body, String dateTimeNotification) throws ParseException {
        Date notification = parseDateTime(dateTimeNotification);
        todoRepository.save(new Todo(headline, body, new Date(), notification));
    }

    @Override
    public boolean updateTodo(String id, String headline, String body, String dateTimeNotification) throws ParseException {
        Optional<Todo> isPresent = todoRepository.findById(id);

        if(isPresent.isPresent()){
            Todo toUpdate = isPresent.get();

            Date notification = parseDateTime(dateTimeNotification);

            toUpdate.setHeadline(headline);
            toUpdate.setBody(body);
            toUpdate.setCreated_datetime(new Date());
            toUpdate.setNotification_datetime(notification);

            todoRepository.save(toUpdate);
            return true;
        }
        return false;
    }

    @Override
    public Todo getTodoById(String id) throws ResourceNotFoundException {
        Optional<Todo> found = todoRepository.findById(id);

        if (!found.isPresent()) throw new ResourceNotFoundException();
        else return found.get();
    }

    @Override
    public Date parseDateTime(String dateTime) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm", Locale.ENGLISH);
        return dateFormat.parse(dateTime);
    }

    @Override
    public boolean deleteTodoById(String id) throws ResourceNotFoundException {
        Todo toDelete = this.getTodoById(id);
        if (!toDelete.equals(null)) {
            todoRepository.delete(toDelete);
            return true;
        }
        return false;
    }
}
