package com.dudak.todo.Todo.service;

import com.dudak.todo.Exceptions.ResourceNotFoundException;
import com.dudak.todo.Todo.model.Todo;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface TodoService {
    List<Todo> getAllTodo();

    void createTodo(String headline, String body, String dateTimeNotification) throws ParseException;

    boolean updateTodo(String id, String todo_headline, String todo_body, String dateTimeNotification) throws ParseException, ResourceNotFoundException;

    Todo getTodoById(String id) throws ResourceNotFoundException;

    Date parseDateTime(String dateTime) throws ParseException;

    boolean deleteTodoById(String id) throws ResourceNotFoundException;
}
