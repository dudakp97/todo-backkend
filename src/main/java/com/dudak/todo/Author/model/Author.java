package com.dudak.todo.Author.model;

import com.dudak.todo.Todo.model.Todo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Size;
import java.util.ArrayList;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "authors")
public class Author {

    /*
     TODO:
    * add email         
    */

    @Id
    private String id;

    @Size(min = 2, max = 100)
    private String name;

    //@DBRef
    private ArrayList<Todo> todos = new ArrayList<>();

    private String email;

    public Author(String name) {
        this.name = name;
    }

    public void addToTodos(Todo todo){
        this.todos.add(todo);
    }
}
