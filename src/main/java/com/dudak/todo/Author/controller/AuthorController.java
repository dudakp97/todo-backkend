package com.dudak.todo.Author.controller;

import com.dudak.todo.Author.model.Author;
import com.dudak.todo.Author.service.AuthorService;
import com.dudak.todo.Exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.List;

/*
@PathVariable ({param})- iba zadam meno objektu, pouzivam pri jednom parametri
bez @PathVariable - zadavam parametra v tvare ?param0=xxx&param1=yyy
*/

@RestController
@RequestMapping(value = "/v1/author")
public class AuthorController {

    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @CrossOrigin
    @GetMapping
    public List<Author> getAllAuthors() {
        return authorService.getAllAuthors();
    }

    //?xyz
    @GetMapping(value = "/{id}")
    public Author getAuthorById(@PathVariable String id) throws ResourceNotFoundException {
        return authorService.getAuthorById(id);
    }

    //?xxx
    @GetMapping(value = "/byName/{name}")
    public Author getAuthorByName(@PathVariable String name) throws ResourceNotFoundException {
        return authorService.getAuthorByName(name);
    }

    //?name=xxx
    @PostMapping
    public ResponseEntity createAuthor(@NotNull String name) throws ResourceNotFoundException {
        boolean isFound = authorService.createAuthor(name);
        return new ResponseEntity(isFound ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    //?name=xxx&todo_messege=yyy&todoNotification=
    @PutMapping(value = "/update")
    public ResponseEntity updateAuthor(String name, String todoHeadline, String todoBody, String todoNotification) throws ResourceNotFoundException, ParseException {
        boolean result = authorService.updateAuthorTodos(name, todoHeadline, todoBody, todoNotification );
        return new ResponseEntity(result ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    //?xxx
    @DeleteMapping("/deleteByName/{name}")
    public ResponseEntity deleteAuthorByName(@PathVariable String name) throws ResourceNotFoundException {
        Author target = authorService.getAuthorByName(name);

        if (target != null) authorService.deleteAuthorByName(name);
        return new ResponseEntity(target != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    //?xxx
    @DeleteMapping("/{id}")
    public ResponseEntity deleteAuthorById(@PathVariable String id) throws ResourceNotFoundException {
        Author target = authorService.getAuthorById(id);

        if (target != null) authorService.deleteAuthorById(id);
        return new ResponseEntity(target != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

}
