package com.dudak.todo.Author.service;

import com.dudak.todo.Author.model.Author;
import com.dudak.todo.Author.repository.AuthorRepository;
import com.dudak.todo.Exceptions.ResourceNotFoundException;
import com.dudak.todo.Todo.model.Todo;
import com.dudak.todo.Todo.repository.TodoRepository;
import com.dudak.todo.Todo.service.TodoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final TodoRepository todoRepository;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository, TodoRepository todoRepository, TodoServiceImpl todoService) {
        this.authorRepository = authorRepository;
        this.todoRepository = todoRepository;
    }

    @Override
    public List<Author> getAllAuthors() { return  authorRepository.findAll(); }

    @Override
    public Author getAuthorById(String id) throws ResourceNotFoundException {
        Optional<Author> found = authorRepository.findById(id);

        if (!found.isPresent()) throw new ResourceNotFoundException();
        else return found.get();
    }

    @Override
    public Author getAuthorByName(String name) throws ResourceNotFoundException {
        Optional<Author> toFind = this.contains(name);
        if (toFind.isPresent()) return toFind.get();
        else throw new ResourceNotFoundException();
    }

    @Override
    public boolean createAuthor(@NotNull String name) {
        Optional<Author> toCreate = this.contains(name);
        if (!toCreate.isPresent()) {
            authorRepository.save(new Author(name));
            return true;
        }
        return false;
    }

    @Override
    public void updateAuthor(@NotNull Author author) {
        authorRepository.save(author);
    }

/*
    @Override
    public boolean updateAuthorTodos(String authorName, String todoHeadline, String todoBody) throws ResourceNotFoundException {
        Optional<Author> toUpdate = this.contains(authorName);
        if (toUpdate.isPresent()){
            Author owner = this.getAuthorByName(authorName);
            Todo toAppend = new Todo(todoHeadline, todoBody, new Date());

            todoRepository.save(toAppend);
            owner.addToTodos(toAppend);
            this.updateAuthor(owner);
            return true;
        }
        else throw new ResourceNotFoundException();
    }
    */

    @Override
    public boolean updateAuthorTodos(String authorName, String todoHeadline, String todoBody, String todoNotification) throws ResourceNotFoundException, ParseException {
        Optional<Author> toUpdate = this.contains(authorName);
        if (toUpdate.isPresent()){
            Author owner = this.getAuthorByName(authorName);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm", Locale.ENGLISH);
            Date notification = dateFormat.parse(todoNotification);
            Todo toAppend = new Todo(todoHeadline, todoBody, new Date(), notification);

            todoRepository.save(toAppend);
            owner.addToTodos(toAppend);
            this.updateAuthor(owner);
            return true;
        }
        else throw new ResourceNotFoundException();
    }

    @Override
    public boolean deleteAuthor(Author author) throws ResourceNotFoundException {
        Optional<Author> toDelete = this.contains(author.getName());
        if (toDelete.isPresent()) {
            todoRepository.deleteAll(
                    author.getTodos()
            );
            authorRepository.delete(author);
            return true;
        }
        throw new ResourceNotFoundException();
    }

    @Override
    public boolean deleteAuthorById(String id) throws ResourceNotFoundException {
        Optional<Author> toDelete = authorRepository.findById(id);
        if (toDelete.isPresent()) {
            todoRepository.deleteAll(
                    toDelete.get().getTodos());
            authorRepository.delete(toDelete.get());
            return true;
        }
        throw new ResourceNotFoundException();
    }

    @Override
    public boolean deleteAuthorByName(String name) throws ResourceNotFoundException {
        Optional<Author> toDelete = this.contains(name);
        if (toDelete.isPresent()) {
            todoRepository.deleteAll(
                    toDelete.get().getTodos()
            );
            authorRepository.delete(toDelete.get());
            return true;
        }
        else throw new ResourceNotFoundException();
    }
/*
    @Override
    public boolean contains(String name) {
        Optional<Author> toFind = authorRepository.findAll()
                .stream()
                .filter(author -> author.getName().equals(name))
                .findFirst();

        if (toFind.isPresent()) return true;
        else {
            return false;
        }
    }
*/
    @Override
    public Optional<Author> contains(String name) {
        return authorRepository.findAll()
                .stream()
                .filter(author -> author.getName().equals(name))
                .findFirst();
    }
}
