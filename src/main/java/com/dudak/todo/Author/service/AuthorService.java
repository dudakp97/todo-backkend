package com.dudak.todo.Author.service;

import com.dudak.todo.Author.model.Author;
import com.dudak.todo.Exceptions.ResourceNotFoundException;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface AuthorService {

    boolean createAuthor(String name) throws ResourceNotFoundException;

    void updateAuthor(Author author);

    //boolean updateAuthorTodos(String name, String todo_headline, String todo_body) throws ResourceNotFoundException;
    boolean updateAuthorTodos(String authorName, String todoHeadline, String todoBody, String todoNotification) throws ResourceNotFoundException, ParseException;

    List<Author> getAllAuthors();

    Author getAuthorById(String id) throws ResourceNotFoundException;

    Author getAuthorByName(String name) throws ResourceNotFoundException;

    boolean deleteAuthor(Author author) throws ResourceNotFoundException;

    boolean deleteAuthorById(String id) throws ResourceNotFoundException;

    boolean deleteAuthorByName(String name) throws ResourceNotFoundException;

    //boolean contains(String name) throws ResourceNotFoundException;
    Optional<Author> contains(String name) throws ResourceNotFoundException;
}
