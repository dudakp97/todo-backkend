package com.dudak.todo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {com.dudak.todo.TodoApplication.class})
public class TodoApplicationTests {

    @Test
    public void contextLoads() {
    }

}

